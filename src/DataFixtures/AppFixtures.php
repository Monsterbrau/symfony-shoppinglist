<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\ProductList;
use App\Entity\Product;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);

        $productList = new ProductList();
        $productList->setName("Test List");
        
        for ($i=0; $i < 5; $i++) { 
            $product = new Product();
            $product->setName("Produit ${i}");
            $product->setTag("Food");
            $product->setComment("This is a comment");
            $product->setQty($i);
            $product->setDone(false);

            $productList->addProduct($product);

            $manager->persist($product);
        }

        $manager->persist($productList);
        $manager->flush();
    }
}
