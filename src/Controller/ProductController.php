<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Product;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class ProductController extends Controller
{
    private $serializer;

    public function __construct(){
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });
        $this->serializer = new Serializer([$normalizer], [$encoder]);
    }

    /**
     * @Route("product/{product}", methods={"DELETE"})
     */
    public function delete(Product $product)
    {
        $manager = $this->getDoctrine()->getManager();
        $manager->remove($product);
        $manager->flush();

        return new Response("OK", 204);
    }

    /**
     * @Route("product/{product}", methods={"PUT"})
     */
    public function update(Product $product, Request $request)
    {
        $content = $request->getContent();
        $updateProduct = $this->serializer->deserialize($content, Product::class, "json");

        $manager = $this->getDoctrine()->getManager();

        $product->setName($updateProduct->getName());
        $product->setTag($updateProduct->getTag());
        $product->setComment($updateProduct->getComment());
        $product->setQty($updateProduct->getQty());
        $product->setDone($updateProduct->getDone());

        $manager->persist($product);
        $manager->flush();
        
        $data = $this->serializer->normalize($product, null, ['attributes' =>['id', 'name', 'tag', 'qty', 'done', 'productList' => ['id', 'name']]]);

        return new Response($this->serializer->serialize($data, "json"));
    }
}
